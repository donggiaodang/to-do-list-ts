import { type } from '@testing-library/user-event/dist/type'
import React from 'react'
import { I_UserData } from './DemoProps';


interface I_UserInfo{
    user: I_UserData;
}

export default function UserInfo({user}: I_UserInfo) {
  return (
    <div>
        <p>{user.name}</p>
        <p>{user.email}</p>
    </div>
  )
}