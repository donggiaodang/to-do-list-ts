import { type } from 'os';
import React from 'react'
import UserInfo from './UserInfo';

// tsrfc => typescript syntax
type Props = {}


export interface I_UserData{
    name: string;
    email: string;
}

let data : I_UserData={
    name: 'alice',
    email: 'alice@gmail.com',
};


export default function DemoProps({}: Props) {
  return (
    <div>
        <UserInfo user={data}/>
    </div>
  )
}