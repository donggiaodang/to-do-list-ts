import React from 'react'
import TodoForm from './TodoForm/TodoForm'
import TodoList from './TodoList'
import { useState } from 'react'
import { I_Todo } from './interfaceExTodos/interfaceExTodos';
type Props = {}

export default function Ex_Todos({}: Props) {
  const [newTodo, setNewTodo]= useState<I_Todo>(Object)
 
  const [todos, setTodos] = useState<I_Todo[]>(
    [{
      id:'1',
      title: 'Du an cuoi khoa',
      isCompleted: true,
    },
    {
      id:'2',
      title: 'lam CV',
      isCompleted: false,
    }]
  )

  const handleAddTodo = (newTodo:I_Todo) => { 
    let newTodos = [...todos,newTodo]
    setTodos(newTodos)
   }
  const handleRemove = (idTodo:string) => { 
    //C1: let cloneTodo = [...todos]
    // let index = cloneTodo.findIndex((i)=>{
    //   return i.id == idTodo
    // })
    // if(index!=-1){
    //   let newTodosArr = cloneTodo.splice(index,1)
    //   return setTodos(newTodosArr)
    // }else{
    //   console.log(Error);
    //   return
    // }

    // C2
    let newTodos = todos.filter((item)=>{
      
      return item.id != idTodo;
    })

    console.log('newTodos:',newTodos);
    setTodos(newTodos)
   }

   const handleCheck = () => { 

    }

  const handleEdit = (idTodo:string) => { 
    // findindex de tim vi tri cua no trong arr roi show object len form 
    // sau khi show len input r lai lay no ve tim index r gan lai gia tri cho no trong Arr(Ly thuyet con thuc hanh thi tu tu) 
    let index = todos.findIndex((todo)=>{
      return todo.id == idTodo
    }) 
    
    
    const newEditTodo = todos[index]
    console.log('newEditTodo: ', newEditTodo);
    setNewTodo(newEditTodo)

   }

   const handleEdittedTodo = (id:string,todo:I_Todo) => { 
    let index = todos.findIndex((todo)=>{
      return todo.id == id
    })
   if(index!=-1){
    todos[index]=todo
    setTodos([...todos])
   }
    }
  return (
    <div className='container mx-auto'>
        <TodoForm todos={todos} newTodo={newTodo} handleAddTodo ={handleAddTodo} handleEdittedTodo={handleEdittedTodo}
       />
        <TodoList handleRemove={handleRemove} todos={todos} handleEdit={handleEdit}/>
    </div>
  )
}