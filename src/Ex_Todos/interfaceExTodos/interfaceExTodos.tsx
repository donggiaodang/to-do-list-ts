export interface I_Todo {
  id: string;
  title: string;
  isCompleted: boolean;
}

export interface I_TodoListComponent {
  todos: I_Todo[];
  handleRemove: (id: string) => void;
  handleEdit: (id: string) => void;
}

export interface I_TodoItemComponent {
  todo: I_Todo;
  handleRemove: (id: string) => void;
  handleEdit : (id:string)
=> void
}

export interface I_TodoFormComponent {
  handleAddTodo: (todo: I_Todo) => void;
  newTodo : I_Todo;
  handleEdittedTodo :
  (id:string,todo:I_Todo)=>void;
  todos:I_Todo[]
}
