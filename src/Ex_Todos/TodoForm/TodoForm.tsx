import { nanoid } from 'nanoid';
import React, { useEffect, useState } from 'react'
import { I_Todo, I_TodoFormComponent } from './../interfaceExTodos/interfaceExTodos';

type Props = {}

export default function TodoForm({handleAddTodo, newTodo,handleEdittedTodo,todos}: I_TodoFormComponent) {

  const [todo,setTodo] = useState<I_Todo>(Object)
  const [title,setTitle]= useState<string>("")
 
  const handleOnChangeTitle = (e: React.ChangeEvent<HTMLInputElement>) => { 
   
 console.log("e.target: ", e.target.value);
  setTitle(e.target.value)
  }
  useEffect(() => { 
    if(!newTodo){
     setTitle("")
     console.log("newtodo: ",newTodo);
    }else{
      setTitle(newTodo.title)
      setTodo(newTodo)
    }
   },[newTodo])
  console.log(title);
   const handleSubmit = () => { 
    let objectTodo: I_Todo={
      id:nanoid(1),
      title:title,
      isCompleted: false,
    }
    const index = todos.findIndex((item)=>{
      return item.id == todo.id
    })
    console.log("index: ", index);
    if(index!=-1){
    handleEdittedTodo(todo.id,{id:todo.id,title:title,isCompleted:false})
    setTitle("")
setTodo({id:"",title:"",isCompleted:false})
    }else{
    handleAddTodo(objectTodo)
    setTitle("")
    }
   }
  
  console.log('todo: ', todo);
  return (
    <div className='flex py-20'>
      <input onChange={handleOnChangeTitle} className='px-5 py-2 grow rounded border border-gray-500' type="text" name="title" id="" value={title} />
      <button onClick={handleSubmit} className='bg-red-500 px-5 py-2  text-white'>Add Todo</button>
    </div>
  )
}